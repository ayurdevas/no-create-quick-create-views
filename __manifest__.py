# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    'name': 'No Create or Quick Create option',
    'summary': "Disable Create or Quick Create option in certain views",
    'description': '',
    'author': 'Roberto Sierra',
    'website': 'https://www.gitlab.com/ayurdevas/no-create-quick-create-views',
    "support": '',
    'category': '',
    'version': '12.0.0.1.0',
    'depends': [
        'mrp',
        'sale',
        'point_equivalence',
        'res_partner_commercial',
    ],
    'data': [
        'views/point_equivalence_views.xml',
        'views/mrp_bom_views.xml',
        'views/sale_views.xml',
        'views/res_partner_views.xml',
    ],
    'license': 'AGPL-3',
    'installable': True,
    'application': False,
}
