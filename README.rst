===============================
No create or quick create views
===============================


.. |badge1| image:: https://img.shields.io/badge/licence-AGPL--3-blue.png
    :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
    :alt: License: AGPL-3

|badge1|

Eliminar la posibilidad de crear o creación rápida de las siguientes vistas:

* Sale Order
* Partner
* BOM
* Point Equivalence


**Table of contents**

.. contents::
   :local:


Credits
=======

Authors
~~~~~~~

* Roberto Sierra <roberto@ideadigital.com.ar>

Maintainers
~~~~~~~~~~~

This module is maintained by Ayurdevas SA.
